
### Dependencias ultilizadas no projeto ###

"dependencies": {
    "@react-native-community/async-storage": "^1.12.1",//ultilizada para salvar dados 
    no aplicativo.
    "@react-navigation/bottom-tabs": "^5.11.8",//ultilizada para criar tabs de navegação 
    na parte inferior.
    "@react-navigation/native": "^5.9.3",//ultilizada para navegação entre componentes.
    "@react-navigation/stack": "^5.14.3",//ultilizada para complementar as navegações.
    "axios": "^0.21.1",//ultilizada para conectar a url base da api.
    "react-native-gesture-handler": "^1.10.3",//complemento das navegações.
    "react-native-screens": "^2.18.0",//complemento das navegações.
    "react-native-vector-icons": "^8.1.0",//ultilizada para exibição de icones.
  },
  
  ### Instruções para instalação do aplicativo ###
  
  instalação via terminal ubuntu:
  1 - Estando na pasta do projeto instale as dependencias requeridas usando - npm install .
  2 - execute o servidor local do react native usando - npm start .
  3 - Agora instale o aplicativo usando - react-native run-android .
  
  
  OBS.1: Não foram ultilizadas dependencias como redux e redux saga por requerer de mais tempo  para analise de atributos genéricos e seus devidos fins para a aplicação, tendo isso em vista, foi ultilizado AsyncStorage para persistencia de dados .
  
  
  OBS.2: A rota de pesquisa foi consumida passando como parâmetro o nome da empresa.
O tipo da empresa não está sendo passado pq para isso seria necessário fazer um filtro por nome do tipo na listagem  para pegar o id do tipo digitado, e então passar como parâmetro para a rota. 
Uma sugestão seria a rota de pesquisa receber um termo e esse termo ser usado pelo backend (SQL) para pesquisar tanto pelo nome do tipo de produto quanto pelo nome da empresa. Assim não seria necessário o front pesquisar o nome da empresa na listagem para obter o id do tipo.

Outra sugestão seria usar funções de filtragem do próprio react, dispensando o consumo da rota de pesquisa, nesse caso.

## Agradecimentos ###
Agradeço pela oportunidade de colocar em prática meus conhecimentos e espero poder somar com a equipe em breve se Deus quiser.


Um forte abraço!

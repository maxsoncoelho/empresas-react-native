import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { useNavigation } from '@react-navigation/native';
import styles from './styles';
import { getUser } from '../../storage';



const MyProfile = () => {

    const navigation = useNavigation();
    const [data, setData] = useState('');



    async function getUserData() {
        const user = await getUser()
        setData(user.investor)
    }

    const handleLogOut = async () => {

        await AsyncStorage.clear().then(() => {
            navigation.navigate('SignIn', { screen: 'Home' })
        })
    }


    useEffect(() => {
        getUserData()
    }, [])

    return (

        <View style={styles.container}>
            <Text style={styles.myProfile}>
                Meu Perfil
           </Text>
            <ScrollView >
                <Text style={styles.profileName}>Nome: {data.investor_name}</Text>
                <Text style={styles.profileEmail}>Email: {data.email}</Text>
                <Text style={styles.profileCountry}>País: {data.country}</Text>
                <Text style={styles.profileCity}>Cidade: {data.city}</Text>
                <Text style={styles.profileBalance}>Minha Carteira: {data.balance}</Text>
                <TouchableOpacity style={styles.buttonClose} onPress={handleLogOut}>
                    <Text style={styles.textClose}>Sair</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    )
}



export default MyProfile;
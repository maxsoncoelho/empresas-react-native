import { StyleSheet } from 'react-native'
 

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding:"5%"
    },
    viewLogo:{
        marginTop:"20%",
        marginVertical:20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo:{
        width:190,
        height:80
    },
    textSlogan:{
        marginBottom: 5,
        fontSize: 20,
        textAlign: "center",
        marginHorizontal: 50,
        color: '#000',
        fontWeight: "bold"
    },
    textLogin:{
        fontSize: 20,
        fontWeight:"bold",
        color: '#000',
        margin: 18
    },
    input:{
        height: 40, 
        borderColor: '#000', 
        borderWidth: 3, 
        borderRadius:20,
        width:230,
        marginBottom:15,
        paddingLeft: 15
    },
    buttonLogin:{
        width:130,
        height:40,
        backgroundColor: '#000', 
        borderWidth: 3, 
        borderRadius:20,
        alignItems:'center',
        justifyContent:'center',
        marginVertical:'2%',
        flexDirection: 'row'
    },
    textButtonLogin:{
        color:'#ffffff',
        fontSize:20, 
    },
    forgot:{
        marginBottom:'10%'
    },
    textForgot:{
        fontSize: 18,
        fontWeight:"bold",
        color: '#000',
        margin: 15
    },
    createAccount:{
        position: 'absolute',
        left: 0,
        bottom: 0,
        right: 0,
        backgroundColor:'#000',
        borderTopWidth: 1,
        borderColor: '#000',
        padding: 16,
        justifyContent: 'center',
        alignItems:'center',
        flexDirection: 'row'
    },
    textAccount:{
        fontWeight:'bold',
        color:'#f0f0f0',
        fontSize: 18,
        marginLeft: 16
    }
})

export default styles;
import axios from 'axios';
import { Alert } from 'react-native';


const api = axios.create({

  baseURL:'https://empresas.ioasys.com.br/api/v1'

})

api.interceptors.response.use(
  response => {
    // Do something with response data
    return response
  },
  error => {

    // Do something with response error

    // You can even test for a response code
    // and try a new request before rejecting the promise

    if (
      error.request._hasError === true &&
      error.request._response.includes('connect')
    ) {
      Alert.alert(
        'Aviso',
        'Não foi possível conectar aos nossos servidores, sem conexão a internet',
        [{ text: 'OK' }],
        { cancelable: false },
      )
    }

    if (error.response && error.response.status && error.response.status === 401) {
      const requestConfig = error.config


      return axios(requestConfig)
    }

    return Promise.reject(error)
  },
)



export default api;
